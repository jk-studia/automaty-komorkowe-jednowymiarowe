public interface IAutomat {
    void recognize(int number);
    void generate(int height, int width);
    void submit();
    void show();
}
