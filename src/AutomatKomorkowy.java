public class AutomatKomorkowy implements IAutomat{

    private int table[][];
    private int height;
    private int width;
    private int system[];

    // funkcja rozpoznająca system automatu komórkowego
    @Override
    public void recognize(int number) {
        system = new int[8];
        for(int i = 7; i >= 0; i--){
            if(number!=0) {
                system[i] = number % 2;
                number = number / 2;
            }
            else
                system[i] = 0;
        }
        for(int i: system){
            System.out.print(i+" ");
        }
        System.out.println();
    }

    // funkcja generująca siatkę
    @Override
    public void generate(int height, int width) {
        table = new int[height][width];
        this.height = height;
        this.width = width;
        for(int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (i == 0) {
                    if (width % 2 == 0) {
                        if (j == (width / 2)-1) table[i][j] = 1;
                    } else if (j == Math.floor(width / 2)) table[i][j] = 1;
                    else
                        table[i][j] = 0;

                } else
                    table[i][j] = 0;
            }
        }
    }

    // funkcja przeprowadzająca symulację
    @Override
    public void submit() {
        int tmp1,tmp2,tmp3;
        for(int i = 1; i < height; i++){
            for(int j = 0; j < width; j++){
                if(j == 0) {
                    tmp1 = table[i-1][width-1];
                    tmp2 = table[i-1][j];
                    tmp3 = table[i-1][j+1];
                }
                else {
                    if (j == width - 1) {
                        tmp1 = table[i - 1][j - 1];
                        tmp2 = table[i - 1][j];
                        tmp3 = table[i - 1][0];
                    }
                    else {
                        tmp1 = table[i - 1][j - 1];
                        tmp2 = table[i - 1][j];
                        tmp3 = table[i - 1][j+1];
                    }
                }
                if(tmp1==1&&tmp2==1&&tmp3==1) table[i][j] = system[0];
                if(tmp1==1&&tmp2==1&&tmp3==0) table[i][j] = system[1];
                if(tmp1==1&&tmp2==0&&tmp3==1) table[i][j] = system[2];
                if(tmp1==1&&tmp2==0&&tmp3==0) table[i][j] = system[3];
                if(tmp1==0&&tmp2==1&&tmp3==1) table[i][j] = system[4];
                if(tmp1==0&&tmp2==1&&tmp3==0) table[i][j] = system[5];
                if(tmp1==0&&tmp2==0&&tmp3==1) table[i][j] = system[6];
                if(tmp1==0&&tmp2==0&&tmp3==0) table[i][j] = system[7];
            }
        }
    }

    // funkcja wyświetlająca siatkę
    @Override
    public void show() {
        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++)
                System.out.print(table[i][j]+" ");
            System.out.println();
        }
    }
}
