import java.util.Scanner;

public class Main {
    public static void main(String args[]){
        System.out.println("Automat komorkowy jednowymiarowy");
        System.out.println("Podaj strategie");
        IAutomat automat = new AutomatKomorkowy();
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        scanner.close();
        automat.recognize(a);
        automat.generate(30,30);
        System.out.println("Przed symulacja");
        automat.show();
        System.out.println("Po symulacji");
        automat.submit();
        automat.show();
    }
}
